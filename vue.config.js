module.exports = {
  /**
   * 文档配置：详情请查看文档 https://cli.vuejs.org/zh/config/#vue-config-js
   * 配置文档中没有写的，都按文档默认值打包
   * 
   * publicPath 部署应用包时的基本 URL。
   * 默认情况下，Vue CLI 会假设你的应用是被部署在一个域名的根路径上，例如 https://www.my-app.com/。
   * 如果你的应用被部署在 https://www.my-app.com/my-app/，则设置 publicPath 为 /my-app/。
   */
  publicPath: '/',
  outputDir: 'dist',
  assetsDir: 'static',
  // 生产环境关闭eslint检查
  lintOnSave: process.env.NODE_ENV !== 'production',
  productionSourceMap: false
}