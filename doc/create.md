# 手动构建Vue Cli v4 项目详细文档

> 本文档主要是记录自己手动构建 `Vue Cli v4` 时的步骤，一方面是为了学习，另一方面是为了排错或者优化构建步骤。

## 工具列表记录

+ yarn：代替 `npm` ，尝试改变新的方式，体验不用的技术。
  + `yarn` 的特点：极其快速、特别安全、超级可靠，从 `npm` 安装软件包并使用相同的包管理流程。
  + [yarn安装步骤请参考官网文档](https://www.yarnpkg.com/zh-Hans/docs)。
  + 使用提示：`yarn` 默认镜像是国外的，需要更改成淘宝镜像。
  ```shell
  # 查看当前使用的镜像源
  yarn config get registry

  # 全局修改 镜像源 为淘宝镜像
  yarn config set registry https://registry.npm.taobao.org/
  ```
  
+ Vue Cli：[请您参照Vue Cli官网文档使用 yarn 安装](https://cli.vuejs.org/zh/guide/installation.html)。

## 安装步骤
1. 在需要创建项目的文件夹下，直接使用 `vue ui` 。
2. 在UI界面中选择需要的技术栈，`babel` 、 `typeScript`、 `Router`、 `Vuex`、 `CSS Pre-processors`等
  ![UI界面](./img/1.png)
3. git 提交规范插件 `vue-cli-plugin-commitlint-release`, [提交规范参考文档](https://github.com/wangjiaojiao77/vue-cli-plugin-commitlint-release)