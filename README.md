# demo-vuecli4
>本项目采用Vue Cli v4构建的项目，自己手动创建一个vue项目，克隆的项目只需按照下方步骤即可运行项目。手动自己搭建项目也有详细文档，点击详细文档即可查看。

## 项目设置
```
yarn install
```

### ⑴ Compiles and hot-reloads for development
```
yarn serve
```

### ⑵ Compiles and minifies for production
```
yarn build
```

### ⑶ Lints and fixes files
```
yarn lint
```

## 手动搭建vue-cli4项目
+ [手动构建vue-cli4项目步骤](./doc/create.md)
